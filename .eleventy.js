const path = require("path")
const {inspect, promisify} = require("util")
module.exports = (eleventyConfig) => {
    eleventyConfig.addCollection("journal", function(collectionApi) {
        return collectionApi.getFilteredByGlob("pages/journal/**/*.md")
    })

    eleventyConfig.addFilter("_data_log", function(value) {
        const valueCloned = {...value};

        if(level >= 1) {
            delete valueCloned.eleventy;
            delete valueCloned.pkg;
            valueCloned.collections = Object.keys(valueCloned.collections);
        }
        
        if(level >= 2) {
            delete valueCloned.collections;
            delete valueCloned.page;
        }

        console.log(valueCloned);
    })

    eleventyConfig.addFilter("_date", function(datetime=new Date(), format="iso") {
        if(datetime === "now") {
            datetime = new Date();
        }
        if (format === "iso") {
            return datetime.toISOString();
        } else if (format === "human") {
            return datetime.toLocaleString("en-US", {
                dateStyle: "long",
                timeStyle: "long"
            }).replace(/:\d{2}([\s\w]+)$/, "$1"); // remove seconds
        } else if (format === "human-short") {
            if((new Date()).getFullYear() === datetime.getFullYear()) {
                return datetime.toLocaleDateString("en-US", {
                    month: "short",
                    day: "numeric"
                });
            } else {
                return datetime.toLocaleDateString("en-US", {
                    dateStyle: "medium"
                });
            }
        } else {
            throw new Error(`desired format "${format}" not recognized`);
        }
    })

    eleventyConfig.addPassthroughCopy({ "static": "/" });

    // eleventyConfig.on("beforeBuild", async () => {
    //     const {render} = require("sass");

    //     render({
    //         file: path.resolve("sass", "main.css"),
    //         outFile: "static/css/style.css"
    //     }, (err, _res) => {
    //         if (err); throw err;
    //     })
    // })

    eleventyConfig.addLinter("microformats", function(content) {
        return
        const isPostPage = this.inputPath.endsWith("journal/20210811214353.md");
        const isFeedPage = this.inputPath.endsWith("index.html");
        if(isPostPage){
            const {mf2} = require("microformats-parser");
            const {items} = mf2(content, {
                baseUrl: "http://localhost:5000",
            });
            console.log(inspect(items[0], false, 9, true));
        }
    });

    return {
        dir: {
            data: "data",
            layouts: "includes/layouts",
            includes: "includes",
            output: "dist"
        },
        htmlTemplateEngine: "njk",
        markdownTemplateEngine: "njk"
    }
}