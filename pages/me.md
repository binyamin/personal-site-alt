---
slug: me
title: About Me
---

# About Me

I am a Jewish ✡ student and web developer from Baltimore, Maryland 🗺️. I enjoy [reading 📚](#Reading) and [listening to music 🎼](#music). You may be wondering what I'm up to [now ⏲️](#now) in life.

## Reading
Here are some books I love:

## Music
Here are some songs I enjoy:

## Now
This is my life, currently: